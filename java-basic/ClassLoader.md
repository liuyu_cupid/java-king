### 初识ClassLoader（类加载器）

一、什么是classloader

classloader负责将class文件加载到jvm中，常用的有有三种

- BootstrapClassLoader

  最顶层的加载类，主要加载JDK的核心类库。%JRE_HOME%\lib下的jar

- ExtentionClassloader

  扩展的类加载器，加载目录%JRE_HOME%\lib\ext目录下的jar包

- AppClassLoader

  加载当前应用的classpath的所有类；

二、双亲委托机制

JVM加载一个class时先查看是否已经加载过，没有则通过父加载器，然后递归下去，直到BootstrapClassLoader，如果BootstrapClassloader找到了，直接返回，如果没有找到，则一级一级返回（查看规定加载路径），最后到达自身去查找这些对象。这种机制就叫做双亲委托。

1. 好处是：

   - 避免重复加载
      A和B都需要加载X，各自加载就会导致X加载了两次，JVM中出现两份X的字节码；

   - 防止恶意加载
      编写恶意类java.lang.Object，自定义加载替换系统原生类；如：自定义java.lang.String

      ```java
      package java.lang;
      public class String {
        public static void main(String[] args) {
          System.out.println("haha");
        }
      }
      ```

      运行：

      ```java
         错误: 在类 java.lang.String 中找不到 main 方法, 请将 main 方法定义为:
         public static void main(String[] args)
      否则 JavaFX 应用程序类必须扩展javafx.application.Application
      ```

   - ClassLoader下的loadClass方法

      ```java
          protected Class<?> loadClass(String name, boolean resolve)
              throws ClassNotFoundException
          {
              // 加锁
              synchronized (getClassLoadingLock(name)) {
                  // 首先, 检查 class 是否已经被加载了
                  Class<?> c = findLoadedClass(name);
                  if (c == null) {
                      long t0 = System.nanoTime();
                      try {
                          if (parent != null) {
                              //  如果父加载器不为null,则交给父加载器加载
                              c = parent.loadClass(name, false);
                          } else {
                              c = findBootstrapClassOrNull(name);
                          }
                      } catch (ClassNotFoundException e) {
                          // ClassNotFoundException thrown if class not found
                          // from the non-null parent class loader
                      }
      
                      if (c == null) {
                          // 如果还是没有找到，则按顺序调用findClass()
                          // to find the class.
                          long t1 = System.nanoTime();
                          c = findClass(name);
      
                          // this is the defining class loader; record the stats
                          PerfCounter.getParentDelegationTime().addTime(t1 - t0);
                          PerfCounter.getFindClassTime().addElapsedTimeFrom(t1);
                          PerfCounter.getFindClasses().increment();
                      }
                  }
                  if (resolve) {
                      resolveClass(c);
                  }
                  return c;
              }
          }
      ```

      上面双亲委托模型，首先从缓存找，然后根据parent是否为null向上委托加载。

      双亲委托只是JVM的规范，是可以通过在自定义ClassLoader时重写loadClass方法打破的。

2. 打破双亲委托：

   - JDK1.2之前已经存在的用户自定义类加载器的实现代码	

   - 基础类又要调用回用户的代码

   - 追求程序动态，如热部署

三、利用ClassLoader实现热部署

1. 自定义ClassLoader

   ```Java
   public class HotClassLoader extends ClassLoader {
   
     // 文件最后修改时间
     private long lastModified;
   
     // 加载class文件的classpath
     private String classPath;
   
     /**
      * 检测class文件是否被修改,如果有就重新加载
      */
     private boolean isClassModified(String filename) {
       File file = getFile(filename);
       if (file.lastModified() > lastModified) {
         return true;
       }
       return false;
     }
   
     public Class<?> loadClass(String classPath, String name) throws ClassNotFoundException {
       this.classPath = classPath;
       if (isClassModified(name)) {
         return findClass(name);
       }
       return null;
     }
   
     /**
      * 获取class文件的字节码
      *
      * @param fileName 类的全名
      */
     private byte[] getBytes(String fileName) {
       byte[] buffer = null;
       FileInputStream in = null;
       try {
         File file = getFile(fileName);
         lastModified = file.lastModified();
         in = new FileInputStream(file);
         buffer = new byte[in.available()];
         in.read(buffer);
         return buffer;
       } catch (Exception e) {
         e.printStackTrace();
       } finally {
         try {
           in.close();
         } catch (IOException e) {
           e.printStackTrace();
         }
       }
       return buffer;
     }
   
     /**
      * 获取class文件的真实路径
      */
     /**
      * 获取class文件的真实路径
      */
     private File getFile(String name) {
       String classFilePath = classPath + name.replace(".", "/") + ".class";
       File file = new File(classFilePath);
       if (file.exists()) {
         return file;
       } else {
         // 如果编译没有成功，也就是class文件还没有被创建，则递归调用，直至class文件被创建
         try {
           TimeUnit.MILLISECONDS.sleep(1);
         } catch (InterruptedException e) {
           e.printStackTrace();
         }
         return getFile(name);
       }
     }
   
     @Override
     protected Class<?> findClass(String name) throws ClassNotFoundException {
       byte[] byteCode = getBytes(name);
       return defineClass(null, byteCode, 0, byteCode.length);
     }
   
   }
   ```

2. 需要被热加载的类

   ```Java
   public class HotHello {
     public void hello(String name) {
       System.out.println("hot deploy success: " + "hello: " + name);
     }
   }
   ```

3. Main方法

   ```Java
   public class HotClient {
   public static void main(String[] args) throws Exception {
       while (true) {
         HotClassLoader loader = new HotClassLoader();
         String classPath = HotHello.class.getResource("/").toString().replace("file:", "");
         System.out.println();// 这地方
         Class<?> clazz = loader.loadClass(classPath, "com.hardess.classloader.hot.HotHello");
         Method method = clazz.getMethod("hello", String.class);
         method.invoke(clazz.getDeclaredConstructor().newInstance(), "Liu");
         // 每隔3秒钟重新加载
         Thread.sleep(3000);
       }
     }
   }
   ```

   运行结果：

   ``````
   hot deploy success: hello: Liu
   hot deploy success: hello: Liu
   hot deploy success: hello: Liu
   hot deploy success: hello boy: Liu
   hot deploy success: hello boy: Liu
   ``````

   可以动态修改，输出内容