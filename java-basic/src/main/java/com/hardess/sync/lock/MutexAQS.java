package com.hardess.sync.lock;

import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.Condition;

public class MutexAQS extends AbstractQueuedSynchronizer {

    private static final long serialVersionUID = -4387327721959839431L;

    // 是否处于占用状态
    @Override
    protected boolean isHeldExclusively() {
        return getState() == 1;
    }

    // 当状态为0的时候获取锁
    @Override
    public boolean tryAcquire(int acquires) {
        assert acquires == 1; // Otherwise unused
        if (compareAndSetState(0, 1)) {
            setExclusiveOwnerThread(Thread.currentThread());
            return true;
        }
        return false;
    }

    // 释放锁，将状态设置为0
    @Override
    protected boolean tryRelease(int releases) {
        assert releases == 1; // Otherwise unused
        if (getState() == 0) {
            throw new IllegalMonitorStateException();
        }
        setExclusiveOwnerThread(null);
        setState(0);
        return true;
    }

    // 返回一个Condition，每个condition都包含了一个condition队列
    Condition newCondition() {
        return new ConditionObject();
    }

}
