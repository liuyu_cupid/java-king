package com.hardess.sync;

import java.util.ArrayList;
import java.util.List;

public class SyncMain {

	public static void main(String[] args) {
		List<Thread> consumers = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			Thread thread = new Thread(new Consumer());
			thread.setName("consumer: " + i);
			consumers.add(thread);
		}
		consumers.forEach(x -> x.start());
	}

}
