package com.hardess.sync;

/**
 * 消费公共资源
 */
public class Consumer implements Runnable {

	@Override
	public void run() {

		System.out.println(Thread.currentThread().getName()+"   "+ PublicResource.resource);
		if (PublicResource.resource > 0) {
			PublicResource.resource--;

		}
	}

}
