package com.hardess.classloader.hot;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class HotClassLoader extends ClassLoader {

  // 文件最后修改时间
  private long lastModified;

  // 加载class文件的classpath
  private String classPath;

  /**
   * 检测class文件是否被修改,如果有就重新加载
   */
  private boolean isClassModified(String filename) {
    File file = getFile(filename);
    if (file.lastModified() > lastModified) {
      return true;
    }
    return false;
  }

  public Class<?> loadClass(String classPath, String name) throws ClassNotFoundException {
    this.classPath = classPath;
    if (isClassModified(name)) {
      return findClass(name);
    }
    return null;
  }

  /**
   * 获取class文件的字节码
   *
   * @param fileName 类的全名
   */
  private byte[] getBytes(String fileName) {
    byte[] buffer = null;
    FileInputStream in = null;
    try {
      File file = getFile(fileName);
      lastModified = file.lastModified();
      in = new FileInputStream(file);
      buffer = new byte[in.available()];
      in.read(buffer);
      return buffer;
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        in.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return buffer;
  }

  /**
   * 获取class文件的真实路径
   */
  private File getFile(String name) {
    String classFilePath = classPath + name.replace(".", "/") + ".class";
    File file = new File(classFilePath);
    if (file.exists()) {
      return file;
    } else {
      // 如果编译没有成功，也就是class文件还没有被创建，则递归调用，直至class文件被创建
      try {
        TimeUnit.MILLISECONDS.sleep(1);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      return getFile(name);
    }
  }

  @Override
  protected Class<?> findClass(String name) throws ClassNotFoundException {
    byte[] byteCode = getBytes(name);
    return defineClass(null, byteCode, 0, byteCode.length);
  }

}
