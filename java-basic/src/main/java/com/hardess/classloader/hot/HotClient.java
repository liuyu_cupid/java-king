package com.hardess.classloader.hot;

import java.lang.reflect.Method;

public class HotClient {


  public static void main(String[] args) throws Exception {
    while (true) {
      HotClassLoader loader = new HotClassLoader();
      String classPath = HotHello.class.getResource("/").toString().replace("file:", "");
      Class<?> clazz = loader.loadClass(classPath, "com.hardess.classloader.hot.HotHello");
      Method method = clazz.getMethod("hello", String.class);
      method.invoke(clazz.getDeclaredConstructor().newInstance(), "Liu");
      // 每隔3秒钟重新加载
      Thread.sleep(3000);
    }
  }

}
