package com.hardess.net;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class NetMain {

	public static void main(String[] args) throws MalformedURLException, IOException {
		Document document = Jsoup.parse(new URL("https://baike.baidu.com/search/word?word=刘裕"), 3000);
		StringBuilder sb = new StringBuilder();
		Elements elements = document.getElementsByClass("para");
		for (Element e : elements) {
			for (Element e2 : e.getAllElements()) {
				sb.append(e2.text());
			}
			sb.append("\r\n");
		}

		System.out.println(sb);

	}

}
