package com.hardess.timeout;

public class RetryDemoTask {

	public static void info(String info, Object message) {
		System.out.println(info.replace("{}", String.valueOf(message)));
	}

	public static void info(String info) {
		System.out.println(info);
	}

	/**
	 * 重试方法
	 * 
	 * @return
	 */
	public static boolean retryTask(String param) {
		info("收到请求参数:{}", param);

		int i = RandomUtils.nextInt(0, 11);
		info("随机生成的数:{}", i);
		if (i < 2) {
			info("为0,抛出参数异常.");
			throw new IllegalArgumentException("参数异常");
		} else if (i < 5) {
			info("为1,返回true.");
			return true;
		} else if (i < 7) {
			info("为2,返回false.");
			return false;
		} else {
			// 为其他
			info("大于2,抛出自定义异常.");
			throw new RemoteAccessException("大于2,抛出自定义异常");
		}
	}

}
