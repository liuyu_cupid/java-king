package com.hardess.timeout;

import java.util.HashMap;
import java.util.Map;

import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

public class SpringRetryMain {
	/**
	 * 重试间隔时间ms,默认1000ms
	 */
	private static long fixedPeriodTime = 1000L;
	/**
	 * 最大重试次数,默认为3
	 */
	private static int maxRetryTimes = 3;
	/**
	 * 表示哪些异常需要重试,key表示异常的字节码,value为true表示需要重试
	 */
	private static Map<Class<? extends Throwable>, Boolean> exceptionMap = new HashMap<>();

	public static void main(String[] args) {
		exceptionMap.put(RemoteAccessException.class, true);

		// 构建重试模板实例
		RetryTemplate retryTemplate = new RetryTemplate();

		// 设置重试回退操作策略，主要设置重试间隔时间
		FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
		backOffPolicy.setBackOffPeriod(fixedPeriodTime);

		// 设置重试策略，主要设置重试次数
		SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy(maxRetryTimes, exceptionMap);

		retryTemplate.setRetryPolicy(retryPolicy);
		retryTemplate.setBackOffPolicy(backOffPolicy);

		Boolean execute = retryTemplate.execute(
				// RetryCallback
				retryContext -> {
					boolean b = RetryDemoTask.retryTask("abc");
					info("调用的结果:{}", b);
					return b;
				}, retryContext -> {
					// RecoveryCallback
					info("已达到最大重试次数或抛出了不重试的异常~~~");
					return false;
				});

		info("执行结果:{}", execute);

	}
	
	public static void info(String info, Object message) {
		System.out.println(info.replace("{}", String.valueOf(message)));
	}

	public static void info(String info) {
		System.out.println(info);
	}
}
