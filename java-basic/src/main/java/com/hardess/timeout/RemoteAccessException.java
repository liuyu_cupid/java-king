package com.hardess.timeout;

public class RemoteAccessException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2767070696158758079L;

	public RemoteAccessException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RemoteAccessException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public RemoteAccessException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public RemoteAccessException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public RemoteAccessException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
