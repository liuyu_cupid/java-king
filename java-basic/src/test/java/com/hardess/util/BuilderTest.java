package com.hardess.util;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.hardess.thread.threadlocal.Profiler;

import lombok.Data;

public class BuilderTest {

	@Test
	public void builder() {
		People p = Builder.of(People::new).with(People::setName, "小明").with(People::setAge, 18).build();
		System.out.println(p);
	}

	@Test
	public void compareToNew() {
		Profiler.begin();
		List<People> ps = new ArrayList<>(1000000);
		for (int i = 0; i < 1000000; i++) {
			People p = new People();
			p.setName("小明");
			p.setAge(18);
			ps.add(p);
		}
		Long end2 = Profiler.end();
		System.out.println("new :" + end2);

		Profiler.begin();
		List<People> ps2 = new ArrayList<>(1000000);
		for (int i = 0; i < 1000000; i++) {
			People p = Builder.of(People::new).with(People::setName, "小明").with(People::setAge, 18).build();
			ps2.add(p);
		}
		Long end = Profiler.end();
		System.out.println("builder :" + end);
	}

	@Data
	class People {
		private String name;

		private int age;
	}
}
