package com.cupid.king.dubbo;

public interface DemoService {

    String sayHello(String name);

}
