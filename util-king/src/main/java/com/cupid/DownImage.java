package com.cupid;

public class DownImage {

	public static void main(String[] args) throws Exception {
		if (args.length == 0) {
			System.out.println("please user: java -jar ***.jar sourceUrl [targetFile]");
		}
		String target = "img.jpg";
		if (args.length > 1) {
			target = args[1];
		}
		DownImageUtils.download(args[0], target);
	}

}
