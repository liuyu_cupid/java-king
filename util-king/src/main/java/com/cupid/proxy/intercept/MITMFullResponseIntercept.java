package com.cupid.proxy.intercept;

import com.github.monkeywie.proxyee.intercept.HttpProxyInterceptPipeline;
import com.github.monkeywie.proxyee.intercept.common.FullResponseIntercept;
import com.github.monkeywie.proxyee.util.HttpUtil;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;

import java.nio.charset.Charset;

public class MITMFullResponseIntercept extends FullResponseIntercept {

    @Override
    public boolean match(HttpRequest httpRequest, HttpResponse httpResponse, HttpProxyInterceptPipeline pipeline) {
        //Insert js when matching to Baidu homepage
        return pipeline.getRequestProto().getHost().contains("baidu.com");
    }

    @Override
    public void handleResponse(HttpRequest httpRequest, FullHttpResponse httpResponse, HttpProxyInterceptPipeline pipeline) {
        //Print raw packet
        System.out.println(httpResponse.toString());
        System.out.println(httpResponse.content().toString(Charset.defaultCharset()));
        //Edit response header and response body
        httpResponse.headers().set("handel", "edit head");
        httpResponse.content().writeBytes("<script>alert('hello proxyee')</script>".getBytes());
    }


}
