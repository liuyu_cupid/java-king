package com.cupid.docx4j.king;

import jakarta.xml.bind.JAXBElement;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.Tbl;
import org.docx4j.wml.Tr;

import java.io.File;
import java.util.List;

public class Demo2 {

    public static void main(String[] args) throws Exception {
        // Load document
        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(new File("D:/aaaa.docx"));
// Load main document part
        MainDocumentPart mainDocumentPart = wordMLPackage.getMainDocumentPart();

        for (Object o : mainDocumentPart.getContent()) {
            if (o instanceof JAXBElement jaxbElement) {
                Object value = jaxbElement.getValue();
                System.out.println(value);

                if (value instanceof Tbl tbl) {
                    List<Object> content = tbl.getContent();
                    System.out.println(content);
                    for (Object object : content) {
                        if (object instanceof Tr tr) {
                            List<Object> content1 = tr.getContent();
                            System.out.println(content1);

                            for (Object o1 : content1) {
                                if(o1 instanceof JAXBElement ja ){
                                    System.out.println(ja.getValue());
                                }
                            }

                        }
                    }

                }

            }

//            System.out.println(o.getClass());
//            System.out.println(o);
        }
//// Extract nodes
//        String textNodesXPath = "//w:t";
//        List<Object> textNodes= mainDocumentPart.getJAXBNodesViaXPath(textNodesXPath, true);
//// Print text
//        for (Object obj : textNodes) {
//            Text text = (Text) ((JAXBElement) obj).getValue();
//            String textValue = text.getValue();
//            System.out.println(textValue);
//        }

    }

}
