package com.cupid.king.resilience4j;

public class Response {

  private int status;

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }
}
