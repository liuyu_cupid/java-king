package com.cupid.king.resilience4j;

import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.vavr.control.Try;

import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.TimeoutException;
import java.util.function.Supplier;

public class CircuitBreakerMain {


  public static void main(String[] args) {
    // Create a custom configuration for a CircuitBreaker
    CircuitBreakerConfig circuitBreakerConfig = CircuitBreakerConfig.custom()
      .failureRateThreshold(50)
      .waitDurationInOpenState(Duration.ofMillis(1000))
      .permittedNumberOfCallsInHalfOpenState(2)
      .slidingWindowSize(2)
      .recordExceptions(IOException.class, TimeoutException.class)
      // .ignoreExceptions(BusinessException.class, OtherBusinessException.class)
      .build();

// Create a CircuitBreakerRegistry with a custom global configuration
    CircuitBreakerRegistry circuitBreakerRegistry =
      CircuitBreakerRegistry.of(circuitBreakerConfig);

    CircuitBreaker circuitBreaker = circuitBreakerRegistry
      .circuitBreaker("name");

    Supplier<String> decoratedSupplier = CircuitBreaker
      .decorateSupplier(circuitBreaker, () -> {
        throw new RuntimeException();
      });

//    decoratedSupplier.
    String result = Try.ofSupplier(decoratedSupplier)
      .recover(throwable -> "Hello from Recovery").get();
    System.out.println(result);

  }

}