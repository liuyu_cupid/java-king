package com.cupid.king.minio;

import io.minio.*;
import io.minio.errors.MinioException;

public class FileUploader {
  public static void main(String[] args) {
    // Create a minioClient with the MinIO server playground, its access key and secret key.
    try (MinioClient minioClient = MinioClient.builder()
      .endpoint("http://192.168.168.100:9000")
      .credentials("eRYg5rjey2RBK6D7HVa6", "LxksZpy41vKLCLWUU0mLfMcviXt5qGQwDIcOhxhc").build()) {
      // Make 'sisi-file' bucket if not exist.
      boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket("sisi-file").build());
      if (!found) {
        // Make a new bucket called 'sisi-file'.
        minioClient.makeBucket(MakeBucketArgs.builder().bucket("sisi-file").build());
      } else {
        System.out.println("Bucket 'sisi-file' already exists.");
      }
      // Upload "C:\Users\liuyu\Documents\诗.txt" as object name '唐诗.txt' to bucket
      // uploadFile(minioClient);
      // removeFile(minioClient);
      downloadFile(minioClient);

    } catch (MinioException e) {
      System.out.println("Error occurred: " + e);
      System.out.println("HTTP trace: " + e.httpTrace());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  /**
   * 上传文件
   */
  private static void uploadFile(MinioClient minioClient) {
    try {
      minioClient.uploadObject(
        UploadObjectArgs.builder()
          .bucket("sisi-file")
          .object("唐诗.txt")
          .filename("C:/Users/liuyu/Documents/诗.txt")
          .build());
      System.out.println("'C:/Users/liuyu/Documents/诗.txt' is successfully uploaded as " + "object '唐诗.txt' to bucket 'sisi-file'.");
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }


  private static void downloadFile(MinioClient minioClient) {
    try {
      minioClient.downloadObject(
        DownloadObjectArgs.builder()
          .bucket("sisi-file")
          .object("唐诗.txt")
          .filename("C:/Users/liuyu/Documents/诗2.txt")
          .build());
      System.out.println("'C:/Users/liuyu/Documents/诗.txt' is successfully uploaded as " + "object '唐诗.txt' to bucket 'sisi-file'.");
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * 删除文件
   */
  private static void removeFile(MinioClient minioClient) {
    try {
      minioClient.removeObject(RemoveObjectArgs.builder()
        .bucket("sisi-file")
        .object("唐诗.txt")
        .build());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}