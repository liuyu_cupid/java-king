package com.cupid.king.minio.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MinioKingSpringbootApplication {

  public static void main(String[] args) {
    SpringApplication.run(MinioKingSpringbootApplication.class, args);
  }

}
