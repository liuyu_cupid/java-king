package com.cupid.solr.king;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) throws SolrServerException, IOException {
        final String solrUrl = "http://cupid.vip:8983/solr/my_core";
        HttpSolrClient build = new HttpSolrClient.Builder(solrUrl)
                .withConnectionTimeout(10000, TimeUnit.MILLISECONDS)
                .withSocketTimeout(60000, TimeUnit.MILLISECONDS)
                .build();

//        add(build);

        delete(build);

        build.commit();
        build.close();
    }

    private static void delete(HttpSolrClient build) throws SolrServerException, IOException {
        build.deleteById("1");
        build.deleteByQuery("*:*");
    }

    private static void add(HttpSolrClient build) throws SolrServerException, IOException {
        SolrInputDocument doc = new SolrInputDocument();
        doc.addField("id", "1");
        doc.addField("name", "小米14");
        doc.addField("price", "2000");
        doc.addField("intro", "小米手机14");
        doc.addField("name", "小米手机");

        build.add(doc);
    }


}