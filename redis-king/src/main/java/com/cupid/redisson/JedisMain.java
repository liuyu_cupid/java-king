package com.cupid.redisson;

import redis.clients.jedis.Jedis;

public class JedisMain {

    public static void main(String[] args) {
        JedisUtils redisUtil = JedisUtils.getRedisUtil();
        for (int i = 0; i < 20; i++) {
            redisUtil.set("name" + i, i + "");
        }

        for (int i = 0; i < 20; i++) {
            redisUtil.del("name" + i);
        }

    }

}
