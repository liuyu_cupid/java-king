package com.hardess.search;

/**
 * 二分查找
 * 
 * 局限性：
 *	1、依赖顺序表结构
 *	2、必须是有序
 * 	3、数据不重复
 * @author liuyu
 *
 */
public class BinarySearch implements Search {

	public int search(int[] a, int val) {
		if (a == null || a.length == 0) {
			return -1;
		}
		var low = 0;
		var high = a.length - 1;

		while (low <= high) {
			int mid = low + (high - low) / 2;
			if (a[mid] == val) {
				return mid;
			} else if (a[mid] < val) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}

		}
		return 0;
	}

}
