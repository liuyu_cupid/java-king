package com.hardess.search;

public interface Search {

	/**
	 * 找寻指定值val在数组a中的位置
	 * 
	 * @param a   数组
	 * @param val 指定值
	 * @return
	 */
	int search(int[] a, int val);

}
