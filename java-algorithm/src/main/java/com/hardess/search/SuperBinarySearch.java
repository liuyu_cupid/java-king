package com.hardess.search;

/**
 * 二分查找-第一次出现的位置
 * 
 * 局限性： 1、依赖顺序表结构 2、必须是有序
 * 
 * @author liuyu
 *
 */
public class SuperBinarySearch implements Search {

	public int search(int[] a, int val) {
		if (a == null || a.length == 0) {
			return -1;
		}
		var low = 0;
		var high = a.length - 1;

		while (low <= high) {
			int mid = low + (high - low) / 2;
			if (a[mid] < val) {
				low = mid + 1;
			} else if (a[mid] > val) {
				high = mid - 1;
			} else {
				if (mid == 0 || a[mid - 1] != val)
					return mid;
				high = mid - 1;
			}

		}
		return 0;
	}

}
