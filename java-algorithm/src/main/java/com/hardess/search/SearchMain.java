package com.hardess.search;

public class SearchMain {

	public static void main(String[] args) {
		int[] a = { 100, 400, 700, 800, 900, 1000 };
		// 二分查找
		Search search = new BinarySearch();
		int res = search.search(a, 700);
		System.out.println(res); // 2

		int[] a2 = { 100, 400, 400, 400, 900, 1000 };
		Search search2 = new SuperBinarySearch();
		int res2 = search2.search(a2, 400);
		System.out.println(res2); // 2
	}

}
