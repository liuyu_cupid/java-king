package com.cupid.shiro.jdbc;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

public class JdbcRealmMain {

    public static void main(String[] args) {

        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUsername("root");
        dataSource.setPassword("123456");
        dataSource.setUrl("jdbc:mysql://127.0.0.1:3306/shiro?useUnicode=true&characterEncoding=utf8");
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");

        JdbcRealm jdbcRealm = new JdbcRealm();
        jdbcRealm.setDataSource(dataSource);
        // 需要开启角色和权限的关联，默认false
        jdbcRealm.setPermissionsLookupEnabled(true);

        SecurityManager securityManager = new DefaultSecurityManager(jdbcRealm);


        SecurityUtils.setSecurityManager(securityManager);

        Subject currentUser = SecurityUtils.getSubject();
        Session session = currentUser.getSession();
//        session.setAttribute("someKey", "aValue");
        if (!currentUser.isAuthenticated()) {
            //collect user principals and credentials in a gui specific manner
            //such as username/password html form, X509 certificate, OpenID, etc.
            //We'll use the username/password example here since it is the most common.
            //(do you know what movie this is from? ;)
            UsernamePasswordToken token = new UsernamePasswordToken("root", "root");
            //this is all you have to do to support 'remember me' (no config - built in!):
            token.setRememberMe(true);


            try {
                currentUser.login(token);
                System.out.println("登陆成功");
                //if no exception, that's it, we're done!
            } catch (UnknownAccountException uae) {
                System.out.println("未知的用户名");
                //username wasn't in the system, show them an error message?
            } catch (IncorrectCredentialsException ice) {
                System.out.println("密码错误");
                //password didn't match, try again?
            } catch (LockedAccountException lae) {
                System.out.println("账号不允许登录");
                //account for that username is locked - can't login.  Show them a message?
                //... more types exceptions to check if you want ...
            } catch (AuthenticationException ae) {
                //unexpected condition - error?
            }
        }
        if (currentUser.hasRole("admin")) {
            System.out.println("拥有admin的角色");
        } else {
            System.out.println("无admin的角色");
        }

        if (currentUser.isPermitted("lightsaber:weild")) {
            System.out.println("拥有权限: " + "lightsaber:weild");
        } else {
            System.out.println("无权限: " + "lightsaber:weild");
        }
        if (currentUser.isPermitted("role:new")) {
            System.out.println("拥有权限: " + "role:new");
        } else {
            System.out.println("无权限: " + "role:new");
        }
        currentUser.logout(); //removes all identifying information and invalidates their session too.


    }


}
