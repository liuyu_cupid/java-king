package com.cupid.shiro.ini;


import lombok.extern.java.Log;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.text.IniRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;


@Log
public class IniShiroMain {

    public static void main(String[] args) {

        IniRealm iniRealm = new IniRealm("classpath:shiro.ini");
        SecurityManager securityManager = new DefaultSecurityManager(iniRealm);


        SecurityUtils.setSecurityManager(securityManager);

        Subject currentUser = SecurityUtils.getSubject();
        Session session = currentUser.getSession();
//        session.setAttribute("someKey", "aValue");
        if (!currentUser.isAuthenticated()) {
            //collect user principals and credentials in a gui specific manner
            //such as username/password html form, X509 certificate, OpenID, etc.
            //We'll use the username/password example here since it is the most common.
            //(do you know what movie this is from? ;)
            UsernamePasswordToken token = new UsernamePasswordToken("root", "root");
            //this is all you have to do to support 'remember me' (no config - built in!):
            token.setRememberMe(true);


            try {
                currentUser.login(token);
                System.out.println("登陆成功");
                //if no exception, that's it, we're done!
            } catch (UnknownAccountException uae) {
                System.out.println("未知的用户名");
                //username wasn't in the system, show them an error message?
            } catch (IncorrectCredentialsException ice) {
                System.out.println("密码错误");
                //password didn't match, try again?
            } catch (LockedAccountException lae) {
                System.out.println("账号不允许登录");
                //account for that username is locked - can't login.  Show them a message?
                //... more types exceptions to check if you want ...
            } catch (AuthenticationException ae) {
                //unexpected condition - error?
            }
        }
        log.info("User [" + currentUser.getPrincipal() + "] logged in successfully.");
        if (currentUser.hasRole("admin")) {
            System.out.println("拥有admin的角色");
        } else {
            log.info("Hello, mere mortal.");
        }

        if (currentUser.isPermitted("lightsaber:weild")) {
            System.out.println("拥有权限" + "lightsaber:weild");
        } else {
            log.info("Sorry, lightsaber rings are for schwartz masters only.");
        }
        if (currentUser.isPermitted("winnebago:drive:eagle5")) {
            System.out.println("拥有权限" + "winnebago:drive:eagle5");
            log.info("You are permitted to 'drive' the 'winnebago' with license plate (id) 'eagle5'.  " +
                    "Here are the keys - have fun!");
        } else {
            log.info("Sorry, you aren't allowed to drive the 'eagle5' winnebago!");
        }
        currentUser.logout(); //removes all identifying information and invalidates their session too.

    }


}
