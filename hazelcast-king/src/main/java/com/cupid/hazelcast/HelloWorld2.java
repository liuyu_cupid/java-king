package com.cupid.hazelcast;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import java.util.Map;

public class HelloWorld2 {

    public static void main(String[] args) {
        Config helloWorldConfig = new Config();
        helloWorldConfig.getNetworkConfig().setPort( 5900 );
        helloWorldConfig.setClusterName("hello-world");


        HazelcastInstance hz = Hazelcast.newHazelcastInstance(helloWorldConfig);

        Map<String, String> map = hz.getMap("my-distributed-map");

        System.out.println(map.get("1"));
        System.out.println(map.get("2"));
        System.out.println(map.get("3"));
    }
}
