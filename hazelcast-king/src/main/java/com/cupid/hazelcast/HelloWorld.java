package com.cupid.hazelcast;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import java.util.Map;

public class HelloWorld {

    public static void main(String[] args) {
        Config helloWorldConfig = new Config();

        helloWorldConfig.getNetworkConfig().setPort( 5900 );
        helloWorldConfig.setClusterName("hello-world");

        HazelcastInstance hz = Hazelcast.newHazelcastInstance(helloWorldConfig);

        Map<String, String> map = hz.getMap("my-distributed-map");
        map.put("1", "John");
        map.put("2", "Mary");
        map.put("3", "Jane");

    }
}
